const radioButtons = document.querySelectorAll('input[type="radio"]');
const sections = document.querySelectorAll('.slide');
const carousel = document.querySelector('section');
const themeColor = document.querySelectorAll('.theme-color');
const active = document.querySelectorAll('.active');

const sectionColors = {
  section_1: "#FFC26F",
  section_2: "#BA6476",
};

radioButtons.forEach((radioButton, index) => {
  radioButton.addEventListener('change', () => {
    sections.forEach((section) => {
      section.classList.add('d-none');
      section.classList.add('fade-out');
    });

    sections[index].classList.replace('d-none', 'd-flex');
    sections[index].classList.replace('fade-out', 'fade-in');

    const currentSection = sections[index].id;
    carousel.style.background = sectionColors[currentSection] || "#C38154";

    const currentSectionColor = sectionColors[currentSection] || "#C38154";

    themeColor.forEach((textColor) => {
      textColor.style.color = currentSectionColor;
    });

    active.forEach((activeLink) => {
      activeLink.style.color = currentSectionColor;
    });
  });
});

